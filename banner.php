<?php
header('Content-Type: image/jpeg');

$bannerNumber = getBannerNumber();

$image = "./banners/banner_{$bannerNumber}.jpg";
echo file_get_contents($image);

/**
 *
 *
 * @return int
 * @throws Exception
 */
function getBannerNumber(): int
{
    return random_int(1, 2);
}
