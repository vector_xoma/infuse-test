<?php
use app\BannerLog;

if (file_exists("vendor/autoload.php")) {
    require_once "vendor/autoload.php";
}
else {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    exit("Unable to load 'vendor/autoload.php' file.");
}

$result = null;
$bannerLog = new BannerLog($_POST);
$result = $bannerLog->saveLog();

if ($result['result'] === false){
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
}
header('Content-type: application/json; charset=utf-8');
echo json_encode($result);



