<?php

return [
    'type' => 'mysql',
    'host' => 'mysqlHost',
    'dbname' => 'databaseName',
    'user' => 'userName',
    'pass' => 'userPassword',
    'charset' => 'utf8',
];
