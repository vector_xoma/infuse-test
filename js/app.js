$(document).ready(function() {
    $("#banner").on('load',sendData());

    function sendData(){
        var request = $.ajax({
            url: "app.php",
            method: "POST",
            cache: false,
            dataType: "json",
            data: {
                userAgent: navigator.userAgent,
                pageUrl: window.location.href
            },
            dataType: "json"
        });

        request.done(function(data) {
            console.log('Result operation. Code: ' + data.code + ' Message: ' + data.message);
        });

        request.fail(function(jqXHR) {
            let message = 'Result operation. Code: ' + jqXHR.status + ' Message: ' + jqXHR.responseText;
            console.log(message);
            $("#error").html(message)
        });
    }
});
