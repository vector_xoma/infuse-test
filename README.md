## How to install

1. Change to the working folder of the project
```bash
cd /patch/to/you/project
```
2. Clone the repository by https or ssh:
```bash
git clone https://bitbucket.org/vector_xoma/infuse-test.git . 
git clone git@bitbucket.org:vector_xoma/infuse-test.git . 
```

3. Install composer dependencies
```bash
composer install
```

4. Copy the preset database access settings file 
```bash
cp config/db_example.php  config/db.php
```

5. Set the correct data for accessing the database in the config/db.php file:
   **host**, **dbname**, **user** and **pass**
```php
return [
    'type' => 'mysql',
    'host' => '127.0.0.1',
    'dbname' => 'databaseName',
    'user' => 'userName',
    'pass' => 'userPassword',
    'charset' => 'utf8',
];
```

6. Create a **Phinx** config file, you can read more about Phinx [here](https://book.cakephp.org/phinx/0/en/intro.html) 
```bash
vendor/bin/phinx init
```
and set the correct data for accessing the database in
```php
'development' => [
    'adapter' => 'mysql',
    'host' => 'localhost',
    'name' => 'development_db',
    'user' => 'root',
    'pass' => '',
    'port' => '3306',
    'charset' => 'utf8',
]
```

7. Apply the migration to create the required table in the database 
```bash
vendor/bin/phinx migrate
```

8. Open your project in a browser, for example 
```
http://localhost/index1.html
```
or 
```
http://localhost/index2.html
```
