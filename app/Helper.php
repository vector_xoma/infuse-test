<?php
namespace app;


class Helper
{
    /**
     * The method returns the IP address of the user
     *
     * @return mixed
     */
    public static function getIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];

        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            return $ip;
        }
    }
}
