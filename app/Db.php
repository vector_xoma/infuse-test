<?php
namespace app;

class Db
{
    private static $instance = null;

    /** @var \PDO */
    public $pdo;

    /** @var string[] */
    private $quotedDataStringFormats = [
        'string',
        'unknown type'
    ];

    /** @var array  */
    private $result = [
        'result' => false,
        'code' => 0,
        'message' => '',
        'data' => null
    ];

    /**
     * Db constructor.
     */
    public function __construct()
    {
        try {
            $settings = $this->getPDOSettings();
            $this->pdo = new \PDO($settings['dsn'], $settings['user'], $settings['pass'], null);
        } catch (\PDOException $e) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
            exit($e->getCode() . " " . $e->getMessage());
        }
    }

    /**
     * @return Db|null
     */
    public static function getInstance(): Db
    {
        if (self::$instance == null) {
            self::$instance = new Db();
        }

        return self::$instance;
    }

    /**
     * PDOSettings
     *
     * @return array
     */
    protected function getPDOSettings(): array
    {
        if (file_exists("config/db.php")) {
            $config = require_once "config/db.php";
        }
        else {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
            exit("Unable to load 'config/db.php' file.");
        }

        try {
            return [
                'dsn' => "{$config['type']}:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}",
                'user' => $config['user'],
                'pass' => $config['pass']
            ];
        } catch (\Exception $e) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
            exit($e->getCode() . " " . $e->getMessage()) ;
        }

    }

    /**
     * Method that prepares and executes a query to the database
     *
     * @param string $query query string (select from, or Update or Insert etc)
     * @param array|null $params An array with key-value pairs. The key is the name of the database field,
     * the value is the data to be written in this field
     * @param string $getResult Parameter responsible for displaying the result (an object with data or
     * an array of objects with query result data)
     * @return array
     */
    public function execute(string $query, array $params = null, string $getResult = 'ONE'): array
    {
        $stmt = $this->pdo->prepare($query);

        $params = $this->prepareParams($params);

        $stmt->execute($params);

        if ($getResult == 'ONE'){
            $data = $stmt->fetchObject();
        }
        else{
            $data = $stmt->fetchAll();
        }

        $this->makeResult($stmt->errorCode(), $stmt->errorInfo()[2], $data);

        return $this->result;
    }

    /**
     * Method for preparing stored data.
     * The method can be extended to other types of data, as well as to other actions with prepared data.
     *
     * @param array $params An array with key-value pairs. The key is the name of the database field,
     * the value is the data to be written in this field
     * @return array
     */
    private function prepareParams(array $params): array
    {
        if (!is_null($params)){
            foreach ($params AS $field => $value){
                if (in_array(gettype($value), $this->quotedDataStringFormats)){
                    $params[$field] = htmlspecialchars($value);
                }

            }
        }
        return $params;
    }

    /**
     * Uniform formatting of the response from the DB class
     *
     * @param string $errorCode
     * @param string $message
     * @param null $data
     */
    private function makeResult(string $errorCode, string $message = null, $data = null)
    {
        if ($errorCode == \PDO::ERR_NONE){
            $this->result = [
                'result' => true,
                'code' => 200,
                'message' => 'success',
                'data' => $data
            ];
        }
        else{
            $this->result = [
                'result' => false,
                'code' => $errorCode,
                'message' => $message,
                'data' => null
            ];
        }
    }
}
