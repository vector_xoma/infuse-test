<?php
/**
 * Model for processing storage and obtaining information about banner display logs
 *
 * Class BannerLog
 * @package app
 * @author Anton Khomenko <vector.xoma@gmail.com>
 */
namespace app;

class BannerLog
{
    /** @var Db  */
    private $db;

    /** @var array  */
    private $data = [];

    /**
     * BannerLog constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->db = Db::getInstance();
        $this->data = $data;
    }

    /**
     * The main public method for saving the blog, starts the chain of checking
     * and adding/updating the banner impressions log
     *
     * @return array
     */
    public function saveLog(): array
    {
        return $this->checkExistingLog();
    }

    /**
     * Method for checking the existence of an entry in the log.
     * If in the selection with the condition of matching IP address, User Agent and page URL - update
     * the record by increasing the number of views by one.
     * If the selection with such a condition is empty - create a new record
     *
     * @return array
     */
    private function checkExistingLog(): array
    {
        $query = "SELECT * FROM banner_log
                    WHERE ip_address = :ip_address
                        AND user_agent = :user_agent
                        AND page_url = :page_url
                 ";

        $queryParams = [
            'ip_address' => Helper::getIp(),
            'user_agent' => $this->data['userAgent'],
            'page_url' => $this->data['pageUrl'],
        ];

        $row = $this->db->execute($query, $queryParams);

        if (!empty($row['data'])){
            return $this->updateLog($row['data']->id, $row['data']->views_count);
        }
        else{
            return $this->setLog();
        }
    }

    /**
     * Method for creating a log entry
     *
     * @return array
     */
    private function setLog(): array
    {
        $query = "INSERT INTO banner_log SET
                    ip_address = :ip_address,
                    user_agent = :user_agent,
                    page_url = :page_url,
                    views_count = :views_count
                 ";

        $queryParams = [
            'ip_address' => Helper::getIp(),
            'user_agent' => $this->data['userAgent'],
            'page_url' => $this->data['pageUrl'],
            'views_count' => 1
        ];

        return $this->db->execute($query, $queryParams);
    }

    /**
     * Method for updating the log entry
     *
     * @param int $id Record ID
     * @param int $countOfViews Current views count
     * @return array
     */
    private function updateLog(int $id, int $countOfViews): array
    {
        $query = "UPDATE banner_log SET
                        views_count = :views_count,
                        view_date = :view_date
                    WHERE id = :id
                 ";

        $queryParams = [
            'views_count' => $countOfViews + 1,
            'view_date' => date('Y-m-d H:i:s'),
            'id' => $id
        ];

        return $this->db->execute($query, $queryParams);
    }
}
