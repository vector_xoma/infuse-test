<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateBannerLogTable extends AbstractMigration
{
    private $tableName = "banner_log";

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table($this->tableName);

        $table->addColumn('ip_address', 'string', ['limit' => 15, 'null' => false])
            ->addColumn('user_agent', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('view_date', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('page_url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('views_count', 'integer', ['default' => 0, 'null' => false])
            ->addIndex(['ip_address', 'user_agent', 'page_url'], ['name' => 'address_agent_url', 'unique' => true])
            ->create();


    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table($this->tableName);
        $table->drop()->save();
    }
}
